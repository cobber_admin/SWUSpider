package org.swu.swuse.model;

/**
 * 爬虫规则抽象模型
 * 
 * @author zhanjingbo
 *
 */
public class WebSpiderConfig {

	/* 配置名称 */
	private String name;
	/* 起始的URL */
	private String url;

	/* URL扩展规则 */
	private String targetRegular;

	/* 内容页匹配规则 */
	private String contentRegular;
	/* 内容标题匹配规则 */
	private String titleRegular;
	/* 内容主题匹配规则 */
	private String textRegular;
	/* 内容时间匹配规则 */
	private String dateRegular;
	/* 日期格式匹配规则 */
	private String sdfRegular;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTargetRegular() {
		return targetRegular;
	}

	public void setTargetRegular(String targetRegular) {
		this.targetRegular = targetRegular;
	}

	public String getContentRegular() {
		return contentRegular;
	}

	public void setContentRegular(String contentRegular) {
		this.contentRegular = contentRegular;
	}

	public String getTitleRegular() {
		return titleRegular;
	}

	public void setTitleRegular(String titleRegular) {
		this.titleRegular = titleRegular;
	}

	public String getTextRegular() {
		return textRegular;
	}

	public void setTextRegular(String textReagular) {
		this.textRegular = textReagular;
	}

	public String getDateRegular() {
		return dateRegular;
	}

	public void setDateRegular(String dateRegular) {
		this.dateRegular = dateRegular;
	}

	public String getSdfRegular() {
		return sdfRegular;
	}

	public void setSdfRegular(String sdfRegulad) {
		this.sdfRegular = sdfRegulad;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "WebSpiderConfig [name=" + name + ", url=" + url + ", targetRegular=" + targetRegular
				+ ", contentRegular=" + contentRegular + ", titleRegular=" + titleRegular + ", textReagular="
				+ textRegular + ", dateRegular=" + dateRegular + ", sdfRegular=" + sdfRegular + "]";
	}

}
