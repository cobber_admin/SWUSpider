package org.swu.swuse.model;

import java.util.Date;

import org.swu.swuse.utils.HTMLSpirit;

/**
 * 网页信息模型
 * 
 * @author zhanjingbo
 *
 */
public class WebPage {
	private int id;
	private String html;
	private String title;
	private String url;
	private String text;
	private Date date;
	private String source;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getHtml() {
		return html;
	}

	public void setHtml(String html) {
		this.html = html;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = HTMLSpirit.delHTMLTag(title);
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

}
