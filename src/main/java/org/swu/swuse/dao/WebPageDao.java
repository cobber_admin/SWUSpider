package org.swu.swuse.dao;

import org.swu.swuse.model.WebPage;
/**
 * 网页模型数据库存储接口
 * @author zhanjingbo
 *
 */
public interface WebPageDao {

	public WebPage getWebPageByUrl(String url);

	public void addWebPage(WebPage webpage);

}
