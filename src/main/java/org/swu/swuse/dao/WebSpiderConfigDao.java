package org.swu.swuse.dao;

import java.util.List;

import org.swu.swuse.model.WebSpiderConfig;

/**
 * 爬虫规则模型数据库读取接口
 * 
 * @author zhanjingbo
 *
 */
public interface WebSpiderConfigDao {
	public List<WebSpiderConfig> getConfigs();
}
