package org.swu.swuse.utils;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringUtils {

	private static ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring.xml");

	public static <T> T getBean(Class<T> t) {
		return applicationContext.getBean(t);
	}
}
