package org.swu.swuse.service;

import java.util.List;
import java.util.TimerTask;

import org.swu.swuse.Pipeline.DataBasePipeline;
import org.swu.swuse.Processor.SpiderProcessor;
import org.swu.swuse.dao.WebSpiderConfigDao;
import org.swu.swuse.model.WebSpiderConfig;
import org.swu.swuse.utils.SpringUtils;

import us.codecraft.webmagic.Spider;

/**
 * 定时按照配置文件更新爬虫数据服务
 * 
 * @author zhanjingbo
 *
 */
public class AutoSpiderService extends TimerTask {

	private WebSpiderConfigDao webSpiderConfigDao;

	public AutoSpiderService() {
		this.webSpiderConfigDao = SpringUtils.getBean(WebSpiderConfigDao.class);
	}

	@Override
	public void run() {
		System.out.println("爬虫开始");
		List<WebSpiderConfig> webSpiderList = this.webSpiderConfigDao.getConfigs();
		for (WebSpiderConfig webSpiderConfig : webSpiderList) {
			System.out.println(webSpiderConfig.getName() + "开始");
			Spider.create(new SpiderProcessor(webSpiderConfig)).addUrl(webSpiderConfig.getUrl())
					.addPipeline(new DataBasePipeline()).thread(5).run();
			System.out.println(webSpiderConfig.getName() + "结束");
		}
	}

}
