package org.swu.swuse.main;

import java.util.Timer;

import org.swu.swuse.service.AutoSpiderService;

/**
 * 自动定时爬虫入口
 * 
 * @author zhanjingbo
 *
 */
public class AutoMain {
	public static void main(String[] args) {
		// 定时器
		Timer timer = new Timer();
		// 设置定时任务，定时周期为24小时
		timer.schedule(new AutoSpiderService(), 0, (long) 24 * 60 * 60 * 1000);
	}
}
